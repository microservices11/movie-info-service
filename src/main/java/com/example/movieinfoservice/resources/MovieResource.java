package com.example.movieinfoservice.resources;

import com.example.movieinfoservice.MovieListDto;
import com.example.movieinfoservice.modal.Movie;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieResource {
    @RequestMapping("/{movieId}")
    public Movie getMovieInfo(@PathVariable("movieId") String movieId){
    return new Movie(movieId,"Lord of the rings");
    }

    @RequestMapping("/")
    public MovieListDto getAllMovieInfo(){
        List<Movie> movies =Arrays.asList(
                new Movie( "2","Inception"),
                new Movie("3","Terminator")
        );

        MovieListDto moviesList = new MovieListDto();
        moviesList.setMovieList(movies);
        return moviesList;
    }
}
